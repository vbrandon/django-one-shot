from django.shortcuts import render, get_object_or_404
from todos.models import TodoList

# Create your views here.


def todo_list(request):

    list = TodoList.objects.all()
    context = {"list": list}
    return render(request, "todos/list.html", context)


def show_list(request, id):

    todo = get_object_or_404(TodoList, id=id)
    return render(request, "TodoList/detail.html")
    context = {
        "todo_object": todo,
    }
    return render(request, "show_list/detail.html", context)
